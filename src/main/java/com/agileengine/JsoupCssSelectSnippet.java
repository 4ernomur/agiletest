package com.agileengine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JsoupCssSelectSnippet {

    private static Logger LOGGER = LoggerFactory.getLogger(JsoupCssSelectSnippet.class);

    private static String CHARSET_NAME = "utf8";

    public static void main(String[] args) throws IOException {

        // Jsoup requires an absolute file path to resolve possible relative paths in HTML,
        // so providing InputStream through classpath resources is not a case

        String sourceResourcePath = args[0];

        String resourcePath = args[1];

        String targetElementId = "make-everything-ok-button";

        String cssQuery = "a";

        Elements hrefsElements = findElementsByQuery(new File(resourcePath), cssQuery).get();

        Element elementToFind = JsoupFindByIdSnippet.findElementById(new File(sourceResourcePath), targetElementId).get();
        List<Attribute> attributesToFind = elementToFind.attributes().asList();
        Map<Element, Integer> resultedMap = new HashMap<>();

        for (Element hrefsElement : hrefsElements) {
            List<Attribute> attributesHrefsElem = hrefsElement.attributes().asList();
            for (Attribute attributeToFind : attributesToFind) {
                if (attributesHrefsElem.contains(attributeToFind)) {
                    addAndCount(resultedMap, hrefsElement);
                }
                List<Attribute> containedAttributes = attributesHrefsElem.stream()
                        .filter(i -> i.getKey().equals(attributeToFind.getKey()) && i.getValue().contains(attributeToFind.getValue()))
                        .collect(Collectors.toList());
                containedAttributes.forEach(i -> addAndCount(resultedMap, hrefsElement));
            }

        }
        System.out.println("Similarity of elements:");

        resultedMap.entrySet().stream().sorted(Comparator.comparingInt(Map.Entry::getValue)).forEach(System.out::println);
        System.out.println();

        Element findedElem = resultedMap.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue)).get().getKey();

        StringBuilder stringBuilder = new StringBuilder();
        Element temp = findedElem;
        System.out.println("Our element is: " + findedElem);

        do {
            if (!stringBuilder.toString().isEmpty()) {
                stringBuilder.append("<");
            }
            stringBuilder.append(temp.tag());
            temp = temp.parent();
        } while (temp != null);
        System.out.println();
        System.out.println(stringBuilder.toString());

    }

    private static Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.select(cssQuery));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    private static void addAndCount(Map<Element, Integer> map, Element element) {
        if (map.containsKey(element)) {
            Integer count = map.get(element) + 1;
            map.put(element, count);
        } else {
            map.put(element, 1);
        }
    }

}